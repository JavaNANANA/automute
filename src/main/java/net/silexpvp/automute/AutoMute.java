package net.silexpvp.automute;

import lombok.Getter;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class AutoMute extends JavaPlugin implements Listener {
    private final String[] words = new String[]{
            "nigger",
            "ez",
            "kys",
            "puto",
            "puta",
            "kill yourself",
            "killyourself",
            "hamachi",
            "papucraft",
            "pelotudo",
            "pelotuda",
            "gilipollas",
            "boludo",
            "boluda",
            "gil",
            "morite",
            "rata",
            "suicidate",
            "raton",
            "ratón"};

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onMessage(AsyncPlayerChatEvent event) {
        if (event.getMessage().equalsIgnoreCase("L")) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tempmute " + event.getPlayer().getName() + " 2h Toxicidad (" + event.getMessage() + ")");
            return;
        }

        for (String string : words) {
            if (event.getMessage().toLowerCase().contains(string)) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tempmute " + event.getPlayer().getName() + " 2h Toxicidad (" + event.getMessage() + ")");
            }
        }
    }
}
